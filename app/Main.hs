{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import Control.Exception.Extra
import Control.Monad.Extra
import Earthquake
import System.Directory hiding (findFile)
import System.Environment
import System.Exit
import Turtle hiding (die, FilePath)

findFile :: [FilePath] -> IO (Maybe FilePath)
findFile = findM (fmap (either (const False) id) . try_ . doesFileExist)

main :: IO ()
main = do
  hsExe <- findFile ["Quakefile.hs","Quakefile.lhs"]
  case hsExe of
    Just file -> do
      e <- proc "runhaskell" [fromString file] empty
      when (e /= ExitSuccess) $ exitWith e
    Nothing -> die "Could not find Quakefile.hs"
