# Earthquake - Algebraic Build Engine

Earthquake is a collection of modular tools for constructiing and executing build graphs, like an LLVM for build systems. Earthquake also comes pre-equipped with an embedded Haskell DSL based on [algebraic-graphs](https://hackage.haskell.org/package/algebraic-graphs). Its aim is to make constructing new build systems a case of transpiling the intended build definitions format into the Earthquake graph construction DSL.

## Usage

Here's a simple Quakefile:

```haskell
import Algebra.Graph
import Earthquake

jq_build = [("autoreconf", ["-ivf"]),
            ("./configure", []),
            ("make", []),
            ("make", ["install"])]

jq = Chunk "jq" "https://github.com/stedolan/jq" "63791b795a984e977e6a45471731a28af252097e" jq_build []

main = quake quakeConfig (vertex jq)
```

That's it! Here's one using `connects`

```haskell
import Algebra.Graph
import Earthquake


binutils_build = [("./configure", ["--prefix=$PREFIX", "--disable-nls", "--disable-werror"])]

gcc_build      = [("./configure", ["--prefix=$PREFIX", "--libdir=$PREFIX/lib", "--disable-bootstrap",
                                   "--with-system-zlib", "--disable-multilib", "--enable-languages=c,c++,fortran"])]

binutils_template x = Chunk x "http://git.baserock.org/git/delta/binutils-tarball" "5500a97a2ad1735db5b35bc51cfb825c1f4c38df" binutils_build []

gcc_template x = Chunk x "http://git.baserock.org/git/delta/gcc-tarball" "3b0dbcfa2e5d12bd80cab1b35f08653d61fe7838" gcc_build []

binutils_stage1 = binutils_template "binutils_stage1"
binutils_stage2 = binutils_template "binutils_stage2"

gcc_stage1 = gcc_template "gcc_stage1"
gcc_stage2 = gcc_template "gcc_stage2"
gcc        = gcc_template "gcc"

minimal_stage1 = edges [(binutils_stage1, gcc_stage1)]
minimal_stage2 = edges [(binutils_stage2, gcc_stage2)]
minimal_stage3 = vertex gcc

minimal = connects [minimal_stage1, minimal_stage2, minimal_stage3]

main = quake quakeConfig minimal
```

## Building

Just run `stack build`. Then cd into examples/simple and run `stack exec quake`

## Advantages over classic Baserock-style YAML

* Often whilst developing systems in YAML alongside a build tool in lockstep, we spot
  repetitions that later turn into format changes. These new abstractions take time to design and implement,
  and updating both the tool and the format schema is expensive. Since we are in Haskell, we can
  abstract any pattern with no such friction, and without any need to involve the library maintainers or
  wait for API changes.

* Haskell will report all type errors, defeating the need for YAML validation.

* Haskell code is always very short, pure, safe, and declarative.

* You can actually fallback to YAML, or any custom specification if you want, all that is
  really required is a record type and a translation function to drag your custom type
  through the Graph functor.
