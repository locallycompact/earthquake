{-# LANGUAGE OverloadedStrings #-}

import Algebra.Graph
import Earthquake

jq_build = [("autoreconf", ["-ivf"]),
            ("./configure", []),
            ("make", []),
            ("make", ["install"])]

jq = Chunk "jq" "https://github.com/stedolan/jq" "63791b795a984e977e6a45471731a28af252097e" jq_build []

rust_build = [("./configure", [])]

binutils_build = [("./configure", ["--prefix=$PREFIX", "--disable-nls", "--disable-werror"])]

gcc_build      = [("./configure", ["--prefix=$PREFIX", "--libdir=$PREFIX/lib", "--disable-bootstrap",
                                   "--with-system-zlib", "--disable-multilib", "--enable-languages=c,c++,fortran"])]

binutils_template x = Chunk x "http://git.baserock.org/git/delta/binutils-tarball" "5500a97a2ad1735db5b35bc51cfb825c1f4c38df" binutils_build []

gcc_template x      = Chunk x "http://git.baserock.org/git/delta/gcc-tarball" "3b0dbcfa2e5d12bd80cab1b35f08653d61fe7838" gcc_build []

binutils_stage1 = binutils_template "binutils_stage1"
binutils_stage2 = binutils_template "binutils_stage2"

gcc_stage1 = gcc_template "gcc_stage1"
gcc_stage2 = gcc_template "gcc_stage2"
gcc        = gcc_template "gcc"

minimal_stage1 = edges [(binutils_stage1, gcc_stage1)]
minimal_stage2 = edges [(binutils_stage2, gcc_stage2)]
minimal_stage3 = vertex gcc

minimal = connects [minimal_stage1, minimal_stage2, minimal_stage3]

arch_x86_64_vars = [("ARCH", "x86_64")]

changeVars vs = fmap (\x -> x{vars = vs})

minimal_x86_64 = changeVars arch_x86_64_vars minimal

varyAt :: Chunk -> Graph Chunk -> Graph Chunk -> Graph Chunk
varyAt chunk g y = y >>= (\i -> if name i == name chunk then connect g (vertex i) else vertex i)

main = do
  quake quakeConfig (vertex jq)
  quake quakeConfig minimal_x86_64
  let wierd_minimal = simplify $ varyAt binutils_stage1 (vertex jq) minimal_x86_64
  quake quakeConfig wierd_minimal
