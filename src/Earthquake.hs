{-# LANGUAGE OverloadedStrings #-}

module Earthquake where

import Algebra.Graph hiding (empty)
import Algebra.Graph.Class (toGraph)
import Algebra.Graph.AdjacencyMap hiding (empty, transpose)
import Control.Foldl as Fold hiding (fold, map, mconcat, mapM_)
import Control.Monad
import Control.Monad.Reader
import Data.Text hiding (empty, transpose)
import Filesystem.Path.CurrentOS hiding (empty)
import Prelude hiding (FilePath)
import Turtle

data Chunk = Chunk {
  name :: Text,
  repo :: Text,
  ref  :: Text,
  exec :: [(Text, [Text])],
  vars :: [(Text, Text)]
} deriving (Eq, Ord, Show)

data CacheConfig = CacheConfig { artifacts :: FilePath, gits :: FilePath, tmp :: FilePath }
  deriving (Eq, Show)

type Artifact      = FilePath
type System        = Graph Chunk
type Image         = Graph Artifact
type BuildCache    = ReaderT CacheConfig Shell
type SystemBuilder = ReaderT System BuildCache

fp2text = fromString . encodeString

clone src dst opts = proc "git" (["clone", "--recursive"] <> opts <> [src, dst]) empty

buildChunk :: Chunk -> SystemBuilder Artifact
buildChunk c@(Chunk name repo ref exec vars) = do
  sys       <- ask
  liftIO . print $ dfs [c] $ toGraph . transpose $ sys
  artifacts <- lift . asks $ artifacts
  gits      <- lift . asks $ gits
  tmp       <- lift . asks $ tmp
  forM_ [artifacts, gits, tmp] mktree
  let withtmp = using . mktempdir tmp . (name <>) . pack
  tmpdir  <- withtmp ""
  instdir <- withtmp ".inst"
  let gitdir = gits </> fromText name
  clone repo (fp2text gitdir) ["--bare"]
  clone (fp2text gitdir) (fp2text tmpdir) []
  sh $ do
    pushd tmpdir
    export "DESTDIR" (fp2text instdir)
    forM_ vars $ uncurry export
    forM_ exec $ uncurry $ flip flip empty . proc
  let dst = fromText $ mconcat [name, pack "-", ref]
  cptree instdir $ artifacts </> dst
  return dst

buildSystem :: System -> BuildCache Image
buildSystem = flip runReaderT <*> traverse buildChunk

quakeConfig = CacheConfig "artifacts" "gits" "/tmp"

quake :: CacheConfig -> System -> IO (Maybe Image)
quake c x = flip fold Fold.head $ runReaderT (buildSystem x) c
